from setuptools import setup
import os

thelibFolder = os.path.dirname(os.path.realpath(__file__))
requirementPath = thelibFolder + '/requirements.txt'
with open(requirementPath) as f:
    install_requires = [line for line in f.read().splitlines() if len(line) > 0]

setup(name="chreport",
    version="0.1",
    description="Reports on data gathered from Cloud Harmony.",
    url="",
    author="tsholtis",
    author_email="plmdevel@gmail.com",
    license="MIT",
    packages=['load','report','parse','utils', 'main'],
    zip_safe=False,
    install_requires=install_requires)