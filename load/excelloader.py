import openpyxl as excelutil
from utils import logger

class ExcelLoader:

        log = logger.Logger()

        def load_excel_file(self, filename):
                if filename:
                        wb = excelutil.load_workbook(filename, read_only=True)
                        self.excel_filename = filename
                        self.excel_wb = wb
                        self._load_excel_data(wb)
                else:
                        self.log.error("No Filename Provided - please provide a valid filename.")

        '''
        Turns excel workbook into a Mapped format per worksheet:

        FORMAT:
        data = { Sheet_Title : { row_number : { column_label : value } } } 
        {
                "Sheet_Title" : 
                { 
                        "0" : { "coordinate" : value, "coordinate2": value }, 
                        "1" : { "coordinate" : value } 
                },

                "Sheet_Title2" : 
                {
                        "0" : { "coordinate" : value, "coordinate2" : value, "coordinate3" : value },
                        "1" : ...,
                        "2" : ...
                },
                ...
        }
        '''
        def _load_excel_data(self, wb):
                data = {}
                for sheet in wb.worksheets:
                        sheet_data={}
                        j = 1
                        for row in sheet.iter_rows():
                                row_data={}
                                for cell in row:
                                        if cell.value is not None:
                                                row_data[cell.coordinate] = cell.value
                                sheet_data[j] = row_data
                                j += 1
                        data[sheet.title] = sheet_data
                self.excel_data = data


        def display_excel_data(self):
                if self.excel_data:
                        for sheet_name, sheet_data in self.excel_data.items():
                                self.log.info("Sheet Name: " + str(sheet_name))
                                for row_number, row_data in sheet_data.items():
                                        self.log.info("Row " + str(row_number) + " data: ")
                                        for column, value in row_data.items():
                                                self.log.info("Column " + str(column) + " value: " + str(value))
                else:
                        self.log.error("No excel data is present in this leader - please load excel data first using 'load_excel_file' and 'load_excel_data'")
                
        def get_data(self):
                return self.excel_data