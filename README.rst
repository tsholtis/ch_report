Report_CH
---------

| This tool takes Cloud Harmony results from a set of spreadsheets in a workbook and reports on the results.
|

| The report follows this flow: 

* Load excel spreadsheets
* Parse excel data into a format that can be worked.
* Sort on specific data points ('Latency' for example).
* Rank on the data points across multiple sites (separate sheets within the workbook).
* Print the results.

| Library Dependencies:

* libxml2 and libxslt (sudo apt-get install python3-lxml)
* libxml2 dev (sudo apt-get install libxml2-dev libxslt-dev python-dev)

| TODO:

* Ranking
* Printing in a nice format.