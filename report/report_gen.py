from utils import logger, excelwriter
import operator
import collections
import functools

import copy

class Reporter:

    log = logger.Logger()
    sorted_dataset = {}
    site_recommended = {}  # format here should be:  { 'SiteA': {'sortkeyA': 'cloudsite'}, { 'sortkeyB': 'cloudsite' } }

    def generate_report(self, data_set, path=None):
        if path:
            reportwriter = excelwriter.ExcelWriter()
            reportwriter.createNewWorkbook(path)

            # show sorted lists for each site on separate tabs
            self.generate_sorted_site_tabs(data_set, 'Latency', 'Lowest', (100,200,300), reportwriter)
            self.generate_sorted_site_tabs(data_set, 'Downlink.*4.*', 'Highest', (1,5,10), reportwriter)

            # show the data in a table against every site with the same ordered cloud locations
            # color code the cloud locations based on 'master' and 'rfs' locations
            # the raw latency data here should remain unsorted in terms of the cloud locations
            # this should mostly match the table you were already using before
            # rows = cloud location
            # columns = on-prem site
            self.generate_raw_summary_table(data_set, 'Latency', 'Lowest', (100,200,300), reportwriter)
            self.generate_raw_summary_table(data_set, 'Downlink.*4.*', 'Highest', (1, 5, 10), reportwriter)

            self.print_recommendations(reportwriter)

            # ranked_data = self._rank_d(data_set, 'Latency', listby='Lowest')
            # self._print_report(ranked_data, reportwriter)
            reportwriter.saveWorkBook()
        else:
            # rank data across site by 'Latency'
            ranked_data = self._rank_d(data_set, 'Latency', listby='Lowest')
            #self._print_report(ranked_data)
        return True
                    

    '''
    Internal method - sorts data on a key and in an order.

    See @SiteData to see the format used for sorting.

    sortkey = sort on key
    sortorder = ascending | descending

    returns the sorted data set
    '''
    def _sort_d(self, dataset, sortkey, listby):
        ascending = True
        if listby=="Highest":
            ascending = False
        for sitedata in dataset:
            # BUG:  This is where there is a problem for some sites
            #       Some sites will parse out bad data using .getsortedServiceData, but some won't
            #
            #  UPDATE:  The sites that came out OK had duplicates on 2 thread, not 4 thread bandwidth
            #           So... the parser is just getting junk data and if that junk data conflicts with any of the 3 being gathered, 
            #           then you will see the junk data in the report.
            # 
            #           So... I need to compensate for the excel parser here.
            self.sorted_dataset[sitedata.sitename] = sitedata.getSortedServiceData(sortkey, ascending)
        
    def generate_sorted_site_tabs(self, dataset, sortkey, listby, lmhrange, reportwriter=None):
        d = copy.deepcopy(dataset)
        self._sort_d(d, sortkey, listby)

        if reportwriter:
            styles = excelwriter.CellStyles()

            for sitename, sitedata in self.sorted_dataset.items():
                # get the first item as the recommended
                if sitename not in self.site_recommended.keys():
                    self.site_recommended[sitename] = []
                if len(sitedata) > 0:
                    self.site_recommended[sitename].append({sortkey : sitedata[0].servicename})

                r = 1
                reportwriter.selectWorksheetByName(str(sitename)+"->"+self._clearInvalidSheetChars(str(sortkey)))
                reportwriter.writetoCell(r, 1, "Cloud Location")
                reportwriter.writetoCell(r, 2, sortkey)
                reportwriter.styleCell(r,1, styles.columnHeaderStyle)
                reportwriter.styleCell(r,2, styles.columnHeaderStyle)
                for servicedata in sitedata:
                    r += 1
                    reportwriter.writetoCell(r, 1, servicedata.servicename)
                    reportwriter.writetoCell(r, 2, servicedata.datavalue)

                    if type(servicedata.datavalue) is str:
                        reportwriter.styleCell(r, 2, styles.lightGreyStyle)
                    else:
                        # unpack lmhrange
                        rng = lmhrange
                        if len(rng) < 3:
                            self.log.error("Range for sylting must be 3 - low, medium, high. No styling applied.")
                            rng = (0,0,0)
                        low, medium, high = rng
                        if servicedata.cmp_val(low) < 0:
                            if listby=="Lowest":
                                reportwriter.styleCell(r, 2, styles.greenStyle)
                            else:
                                reportwriter.styleCell(r, 2, styles.redStyle)
                        elif servicedata.cmp_val(low)>=0 and servicedata.cmp_val(high)<1:
                            reportwriter.styleCell(r, 2, styles.yellowStyle)
                        else:
                            if listby=="Lowest":
                                reportwriter.styleCell(r, 2, styles.redStyle)
                            else:
                                reportwriter.styleCell(r, 2, styles.greenStyle)
                
                # adjust sheet column size
                reportwriter.adjustSheetSize(reportwriter.ws)

    def generate_raw_summary_table(self, dataset, sortkey, listby, lmhrange, reportwriter=None):
        d = copy.deepcopy(dataset)
        self._sort_d(d, sortkey, listby)

        if reportwriter:
            # get the list of all cloud locations ran across all customer sites
            cloud_locations = []
            for _, sitedata in self.sorted_dataset.items():
                for servicedata in sitedata:
                    if str(servicedata.servicename) not in cloud_locations:
                        cloud_locations.append(str(servicedata.servicename))
            
            # report on all cloud locations from each site
            # rows = cloud locations
            # columns = customer sites
            if len(cloud_locations) > 0:
                reportwriter.selectWorksheetByName("Consolidated Data - " + self._clearInvalidSheetChars(str(sortkey)))
                r = 2

                styles = excelwriter.CellStyles()
                reportwriter.writetoCell(1,1,'Cloud Locations')
                reportwriter.styleCell(1,1, styles.columnHeaderStyle)

                # write rows - ordering will be the same for all sitenames
                for location in cloud_locations:
                    reportwriter.writetoCell(r, 1, location)
                    reportwriter.styleCell(r, 1, styles.standardCellStyle)
                    r += 1

                # write columns
                c = 1
                for sitename, sitedata in self.sorted_dataset.items():
                    c += 1  # bump over one to keep a table format (column 1 = cloud servicenames)
                    reportwriter.writetoCell(1, c, str(sitename))
                    reportwriter.styleCell(1, c, styles.columnHeaderStyle)  
                    # write data into columns
                    for servicedata in sitedata:
                        # find the row to write to (will always be + 1 since sitename is row 1)
                        try:
                            r = cloud_locations.index(str(servicedata.servicename)) + 2
                            reportwriter.writetoCell(r, c, servicedata.datavalue)

                            if type(servicedata.datavalue) is str:
                                reportwriter.styleCell(r, c, styles.lightGreyStyle)
                            else:
                                rng = lmhrange
                                if len(rng) < 3:
                                    self.log.error("Range for sylting must be 3 - low, medium, high. No styling applied.")
                                    rng = (0,0,0)
                                # unpack
                                low, medium, high = rng

                                if servicedata.cmp_val(low) < 0:
                                    if listby=="Lowest":
                                        reportwriter.styleCell(r, c, styles.greenStyle)
                                    else:
                                        reportwriter.styleCell(r, c, styles.redStyle)
                                elif servicedata.cmp_val(low) >= 0 and servicedata.cmp_val(high) < 1:
                                    reportwriter.styleCell(r, c, styles.yellowStyle)
                                else:
                                    if listby=="Lowest":
                                        reportwriter.styleCell(r, c, styles.redStyle)
                                    else:
                                        reportwriter.styleCell(r, c, styles.greenStyle)
                        except:
                            # skip - but this shouldn't happen - we collected all the possible service names
                            self.log.error("A ServiceName was missed in collection! - This should not happen!!")
                
                # adjust column size
                reportwriter.adjustSheetSize(reportwriter.ws)
                        
    def print_recommendations(self, reportwriter=None):
        if reportwriter:
            reportwriter.selectWorksheetByName("Sheet 1")  # get the default sheet
            reportwriter.sheetname = "Site Recommendations"
            r = c = 1
            for sitename, rec_data in self.site_recommended.items():
                r = r + 3  # increment rows on each pass
                c = 1  # reset columns on each pass
                reportwriter.writetoCell(r+1,c,sitename)
                for datapoint in rec_data:
                    # write data points
                    for k,v in datapoint.items():
                        c = c + 1
                        reportwriter.writetoCell(r,c,k)
                    # write data values
                    c = 1
                    for k,v in datapoint.items():
                        c = c + 1
                        reportwriter.writetoCell(r+1,c,v)


    def show_sorted_site_list(self, dataset, sortkey, listby, reportwriter=None):
        if not self.sorted_dataset:
            self._sort_d(dataset, sortkey, listby)
                
        if reportwriter:
            r = c = 1
            for sitename, sitedata in self.sorted_dataset.items():
                c += 1
                reportwriter.writetoCell(1,c,str(sitename))
                for servicedata in sitedata:
                    r += 1
                    reportwriter.writetoCell(r,1,servicedata.servicename)
                    reportwriter.writetoCell(r,c,servicedata.datavalue)
        else:
            stream = self.log.info
            for sitename, sitedata in self.sorted_dataset.items():
                stream("SiteName: " + str(sitename))
                for servicedata in sitedata:
                    stream(servicedata.servicename)
                    stream(servicedata.dataname)
                    stream(servicedata.datavalue)


    def _clearInvalidSheetChars(self, sheetname):
        return sheetname.replace("\\","") \
                        .replace("/", "") \
                        .replace("*","") \
                        .replace("[","") \
                        .replace("]","") \
                        .replace(":","") \
                        .replace("?","") \


    ''' 
    Internal method - ranks data on 'rankby' and then lists by 'listby'.

    data_set = data set to rank
    rankby = rank by a specific key across all data_set entries
    listby = highest | lowest

    returns the sorted data_set
    '''
    def _rank_d(self, data_set, rankby, listby):
        
        if not self.sorted_dataset:
            if listby == 'highest':
                self._sort_d(data_set, rankby, False)
            else:
                self._sort_d(data_set, rankby, True)
        else:
            self.log.info("Sorted data set found already! Using sorted data set available.")

        # returns a list - maybe multiple sites if no overlap occurs
        self.log.info("Finding master site...")
        master_server_regions = self._find_master_server(self.sorted_dataset, False)
        self.log.info("Found master site")

        # if more than 1 master region, then check against each master site
        for master_region in master_server_regions:
            self.log.info("Finding high latency sites for : ")
            high_latency_against_master = self._find_high_latency_sites([master_region], self.sorted_dataset, 100, False)
            self.log.info("High Latency sites found: ")
            # TODO:  Some processing to get a true list of sites where latency is high 

        high_latency_sites = high_latency_against_master
        alternate_sites = self._find_alternate_sites(high_latency_sites, self.sorted_dataset, False)

        return { "master": master_server_regions, "rfs": alternate_sites }
    '''
    Internal method - finds the master data given a list of sorted site data.
    
    sorted_site_data:
    format:  { sitename: [ ServiceData ] }
    ServiceData:  object (dataname, datavalue, servicename)
                    filtered on dataname
                    sorted on datavalue

    ascending:
    How to interpret the ordering.
    ascending=True = highest value is desired
    ascending=False = lowest value is desired

    @return:
        SiteData (best found based on parameters)
    '''
    def _find_master_server(self, sorted_site_data, ascending=True):

        bestdata = []

        # 1. Take the top 3 from each site
        for _, sitedataset in sorted_site_data.items():
            bestdata.extend(sitedataset[:3])

        # 2. Find the most common occurrence of the top 3
        service_occurence = {}
        for servicedata in bestdata:
            if servicedata.servicename in service_occurence:
                service_occurence[servicedata.servicename] = service_occurence[servicedata.servicename] + 1
            else:
                service_occurence[servicedata.servicename] = 1

        # 3. If multiple, take the site with the best datavalue (based on ascending or descending)
        most_frequent_occur = 0
        best_service = ""
        duplicates = 0
        for service, occurrence in service_occurence.items():
            if occurrence > most_frequent_occur:
                most_frequent_occur = occurrence
                best_service = service
                duplicates = 1
            elif occurrence == most_frequent_occur:
                duplicates += 1

        # 4. If no clear site, return multiple results
        if duplicates == len(service_occurence.keys()):
            return list(service_occurence.keys())  # need to figure out how to return the service data here
        else:
            return [best_service]


    '''
    Internal method - finds sites with high latency against the master site.

    '''
    def _find_high_latency_sites(self, master_regions, sorted_site_data, control_point, ascending):

        # 1. Find sites in 'sorted_site_data' where the value is above or below the 'control_point' for all of the sites in 'master_regions'
        high_latency_sites = {}
        outside_range_count = 0
        for site, sitedata in sorted_site_data.items():
            if sitedata:
                # check all the service data for a site and find the master regions data
                for servicedata in sitedata:
                    if servicedata.servicename in master_regions:
                        # if a master region has a value outside the control point, then count it as outside the range
                        if ascending and servicedata.cmp_val(control_point) < 1:
                            outside_range_count += 1
                        elif servicedata.cmp_val(control_point) > 1:
                            outside_range_count += 1
                # if all master regions showed a control point outside the range, then this is a high latency site
                if outside_range_count >= len(master_regions):
                    high_latency_sites[site] = sitedata
        
        return high_latency_sites


    ''' 
    Internal method - find alternate sites with lower latency for the provided high latency sites.

    '''
    def _find_alternate_sites(self, high_latency_sites, sorted_site_data, asending):

        # 1. For the 'high_latency_site', find the options where the datavalue is highest or lowest based on 'ascending'
    # for site, sitedata in high_latency_sites.items():



        alternate_sites = sorted_site_data  # TODO:  should return a map of the high latency site to an alternate region data (set of results)

        return alternate_sites



    '''
    Internal method - prints a data set.

    data_set = data set to print out.
    '''
    def _print_report(self, data_set, reportwriter=None):
        for key,val in data_set.items():
            if key == 'master':
                self._print_master_site_info(key,val, reportwriter)
            elif key == 'rfs':
                self._print_header(key, reportwriter)
                self._print_rfs_site_data(val, reportwriter)
            else:
                print("ERROR:  Unknown Key for Report - should be 'master' or 'rfs'")
        

    def _print_header(self, header, reportwriter=None):
        if reportwriter:
            # do the cell out for the header - new sheet?
            pass
        else:
            self.log.info(header)

    def _print_master_site_info(self, key, mastersite, reportwriter=None):
        if len(mastersite) > 1:
            if reportwriter:
                pass
            else:
                self.log.info('No Cloud Site covers more than 1 user site. Please select from the list the site where the majority of users are located:')
                for master in mastersite:
                    self.log.info(master)
        else:
            if reportwriter:
                pass
            else:
                self.log.info('The following Cloud Site was detected as the best for the most user sites:')
                self.log.info(mastersite[0])


    def _print_rfs_site_data(self, site_data, reportwriter=None):
        if reportwriter:
            pass
        else:
            for site, data in site_data.items():
                self.log.info("Site:  " + str(site))
                self.log.info("Best Location for RFS: " + str(data[0].servicename))