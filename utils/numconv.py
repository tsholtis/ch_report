'''
Utility methods to deal with string to numbers.
'''

def isNumber(s: str):
    return s.lstrip('-').replace('.','').isdigit()

def convToFloat(s: str):
    return float(s)