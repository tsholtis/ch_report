import logging
import os
import sys
from enum import IntEnum

class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]

class Logger(object, metaclass=Singleton):

    class LOG_LEVELS(IntEnum):
        INFO = logging.INFO
        WARN = logging.WARN
        ERROR = logging.ERROR
        CRITICAL = logging.CRITICAL
        DEBUG = logging.DEBUG
        ALL = logging.NOTSET


    _LOG_LEVEL = LOG_LEVELS.ALL

    LOG_FILE = "ch_report.log"
    # TODO:  This creates the log file way deep down in the virtual environment library - need to fix. 
    LOG_DIR = os.path.abspath(os.path.dirname(__file__)) + os.path.sep + ".." + os.path.sep + "log"
    _LOG_SETUP = False

    def _log_setup(self):
        if not os.path.exists(self.LOG_DIR):
            os.makedirs(self.LOG_DIR)
            print("Log directory created at: " + self.LOG_DIR)
        # root logger needs set as it is only set at Warn
        logging.root.setLevel(self._LOG_LEVEL)
        print("Log file is being created...")
        lformat = '%(asctime)s - %(module)s - %(levelname)s:   %(message)s'
        lformatter = logging.Formatter(lformat)
        lfh = logging.FileHandler(self.LOG_DIR + "/" + self.LOG_FILE, mode='w', encoding='utf-8', delay=False)
        lfh.setFormatter(lformatter)
        self._logger = logging.getLogger("default")
        self._logger.setLevel(self._LOG_LEVEL)
        self._logger.addHandler(lfh)
        Logger._LOG_SETUP = True

    def reset_global_log_level(self, lvl):
        if lvl not in self.LOG_LEVELS:
            print("Invalid log level specified - please use LOG_LEVELS only.")
        else:
            self._LOG_LEVEL = lvl
            self._logger.setLevel(lvl)

    def info(self, msg):
        if not Logger._LOG_SETUP:
            self._log_setup()
        self._log(msg, "info")

    def warn(self, msg):
        if not self._LOG_SETUP:
            self._log_setup()
        self._log(msg, "warn")

    def error(self, msg):
        if not self._LOG_SETUP:
            self._log_setup()
        self._log(msg, "error")

    def debug(self, msg):
        if not self._LOG_SETUP:
            self._log_setup()
        self._log(msg, "debug")

    def critical(self, msg):
        if not self._LOG_SETUP:
            self._log_setup()
        self._log(msg, "critical")


    def _log(self, msg, lvl):
        if lvl == "info":
            self._logger.info(msg)
        elif lvl == "warn":
            self._logger.warning(msg)
        elif lvl == "debug":
            self._logger.debug(msg)
        elif lvl == "error":
            self._logger.error(msg)
        elif lvl == "critical":
            self._logger.critical(msg)

    def shutdown(self):
        print("this got called - logger shutdown.")
        logging.shutdown()

    def __del__(self):
        self.shutdown()