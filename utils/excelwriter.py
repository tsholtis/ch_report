import openpyxl as excelutil
from openpyxl.styles import Color, PatternFill, NamedStyle, Border, Alignment, Font, Side
from utils import logger

class ExcelWriter:

    log = logger.Logger()
    
    def createNewWorkbook(self, path):
        self.log.info("Creating new workboook...")
        # excelutil.Workbook(write_only=True) is an optimization here but it changes how cells are added
        # in write_only mode, cells must be created as WriteOnlyCells and must be added sequentially (not by location)
        self.wb = excelutil.Workbook()
        self.path = path
        self.selectDefaultWorksheet()

    def selectDefaultWorksheet(self):
        self.ws = self.wb.active

    def selectWorksheetByName(self, name):
        if name in self.wb.sheetnames:
            self.ws = self.wb[name]
        else:
            self.ws = self.wb.create_sheet(str(name))
    
    def saveWorkBook(self):
        if self.path:
            self.wb.save(self.path)
        else:
            raise FileNotFoundError(self.path)

    def writetoCell(self, row, column, data):
        if not self.ws:
            self.selectDefaultWorksheet()
        if row and column:
            self.ws.cell(row=row, column=column, value=data)

    def styleCell(self, row, column, style):
        if not self.ws:
            self.selectDefaultWorksheet()
        if row and column:
            cell = self.ws.cell(row=row, column=column)
            cell.style = style

    def adjustSheetSize(self, sheet):
        # set the columns width for each sheet (site)
        for column_cells in sheet.columns:
            length = max(len(str(cell.value)) for cell in column_cells)
            sheet.column_dimensions[column_cells[0].column].width = length


class ExcelColors:
    BLACK = '00000000'
    WHITE = 'ffffffff'
    RED = '00ff9999'
    GREY = '00dddddd'
    YELLOW = '00ffff99'
    GREEN = '0099ff99'


class CellStyles:
    headerBorder = Border(left=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None), 
                          right=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None),
                          top=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None),
                          bottom=Side(style='thick', color=Color(rgb=ExcelColors.BLACK), border_style=None))

    standardBorder = Border(left=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None), 
                            right=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None),
                            top=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None),
                            bottom=Side(style='thin', color=Color(rgb=ExcelColors.BLACK), border_style=None))

    redStyle = NamedStyle(name='redfill', 
                          fill=PatternFill(patternType='solid', 
                          fgColor=Color(rgb=ExcelColors.RED)), 
                          border=standardBorder)
    lightGreyStyle = NamedStyle(name='greyfill', 
                                fill=PatternFill(patternType='solid', fgColor=Color(rgb=ExcelColors.GREY)), 
                                border=standardBorder)
    yellowStyle = NamedStyle(name='yellowfill', 
                             fill=PatternFill(patternType='solid', fgColor=Color(rgb=ExcelColors.YELLOW)), 
                             border=standardBorder)
    greenStyle = NamedStyle(name='greenfill', 
                            fill=PatternFill(patternType='solid', fgColor=Color(rgb=ExcelColors.GREEN)),
                            border=standardBorder)

    columnHeaderStyle = NamedStyle(name='columnheader', 
                                  fill=PatternFill(patternType='solid', fgColor=Color(rgb=ExcelColors.GREY)), 
                                  border=headerBorder, 
                                  alignment=Alignment(horizontal='center', wrapText=False, vertical='center'),
                                  font=Font(name='Arial', bold=True, italic=False, underline='single', size=10))

    standardCellStyle = NamedStyle(name='standardcell', 
                                   border=standardBorder)



