from utils import logger
from utils import numconv
import functools
import re

AWS_INSTANCES='EC2'
AZURE_INSTANCES='Microsoft Azure Virtual Machines'

DATA_TABLE_HEADERS='Service'

class ServiceData:
    def __init__(self, servicename="",dataname="", datavalue=None):
        self.servicename = servicename
        self.dataname = dataname
        self.datavalue=datavalue
    
    def isaData(self, dataname=''):
        return self.dataname == dataname

    def match(self, dataname=''):
        p = re.compile(dataname)
        if p.match(self.dataname) is not None:
            return True
        else:
            return False

    # FIX:  This fixes the junk data issue where we get 1 or more service data for the same service name, most of which are junk
    #       The junk data we are looking at is where datavalue == servicename (servicetype input)
    #
    def notJunk(self):
        return (not (self.datavalue == self.servicename))

    def cmp_data(self, sdata2):
        return self.cmp_val(sdata2.datavalue)

    def cmp_val(self, val):
        return self.cmp_vals(self.datavalue, val)

    def cmp_vals(self, val1, val2):
        numcmp1 = 0.0
        numcmp2 = 0.0
            
        # handle val1 conversion
        if type(val1) is str and numconv.isNumber(val1):
            numcmp1 = numconv.convToFloat(val1)
        elif type(val1) is int or type(val1) is float:
            numcmp1 = float(val1)
        elif type(val1) is str and not numconv.isNumber(val1):
            numcmp1 = -0.0
        elif type(val1) is None:
            numcmp1 = -0.0
        else:
            numcmp1 = -0.0

        # handle val2 conversion
        if type(val2) is str and numconv.isNumber(val2):
            numcmp2 = numconv.convToFloat(val2)
        elif type(val2) is int or type(val2) is float:
            numcmp2 = float(val2)
        elif type(val2) is str and not numconv.isNumber(val2):
            numcmp2 = -0.0
        elif type(val2) is None:
            numcmp2 = -0.0
        else:
            numcmp2 = -0.0

        if numcmp1==-0.0 or numcmp2==-0.0:
            return 0
        elif numcmp1 > numcmp2:
            return 1
        elif numcmp1 < numcmp2:
            return -1
        else:
            return 0


class SiteData:
    log = logger.Logger()

    def __init__(self, sitename=""):
        self.sitename = sitename
        self.servicedata=[]

    def addServiceData(self,servicename="",dataname="",datavalue=""):
        self.servicedata.append(ServiceData(servicename, dataname, datavalue))

    def getSortedServiceData(self, servicetype, ascending=True):
        self.servicedata = self.filterServiceType(servicetype)
        self.servicedata.sort(key=functools.cmp_to_key(self._cmp_servicedata), reverse=not ascending)
        return self.servicedata
    
    def filterServiceType(self, servicetype):
        # try exact match
        self.log.info("trying an exact match")
        vals = [x for x in self.servicedata if (x.isaData(servicetype) and x.notJunk())]
        if len(vals) == 0:
            # try regex match
            self.log.info("trying a regex match")
            vals = [x for x in self.servicedata if (x.match(servicetype) and x.notJunk())]
        return vals

    
    def _cmp_servicedata(self, d1: ServiceData, d2):
        if type(d2) is ServiceData:
            return d1.cmp_data(d2)
        else: 
            return d1.cmp_val(d2)
    

class Parser: 

    parsed_raw_data = {}
    site_data = []

    log = logger.Logger()  # it's this one actually that becomes incorrect (no _logger)

    # Formats loaded data like this:
    #
    #  {
    #      Site (Tab):  [
    #                      {
    #                         "Cloud Site Name": name,
    #                         "DNS Query" : result,
    #                         "Downlink [1 - 128KB / 4 threads]": result,
    #                         "Downlink [256KB - 10MB / 2 threads]": result,
    #                         "Latency": result
    #                      }
    #                      {
    #                         SAME OBJECT FIELDS AS ABOVE..
    #                      }
    #                   ],
    #
    #      Site (Tab):  [
    #                       MULTIPLE CLOUD SITES AS ABOVE...
    #                   ]
    #  }
    def parse_chrows(self, data):
        for sheet,sheet_data in data.items():
            parsed_sheet_data = []
            self.log.info("sheet = " + str(sheet))

            #--- Pass 1 -> Identify rows with needed data ---#
            header_row = {}
            data_rows = []
            for row,row_data in sheet_data.items():
                for _, cell_value in row_data.items():
                    # look for the data table headers
                    if str(cell_value).find(DATA_TABLE_HEADERS) != -1:
                        # gather the header row ref
                        header_row = row_data
                    # look for AWS EC2 instance rows
                    if str(cell_value).find(AWS_INSTANCES) != -1:
                        # gather the data from this row - need to get the next 4 cells
                        data_rows.append(row_data)
                    # look for Azure Virtual Machine instance rows
                    elif str(cell_value).find(AZURE_INSTANCES) != -1:
                        # gather the data from this row - need to get the next 4 cells
                        data_rows.append(row_data)
            
            #--- Pass 2 -> Get data from the rows identified ---#
            headers = []
            for _, cell_value in sorted(header_row.items()):  # using cell location keys to sort
                headers.append(cell_value)

            for row in data_rows:
                if len(row.items()) != len(headers):
                    self.log.warn("Row data is not the same length as headers - some data may be missing or truncated!")
                i = 0
                data_item = {}
                for _, cell_value in sorted(row.items()):   # using cell location keys to sort
                    if i < len(headers):
                        if type(cell_value) is str and numconv.isNumber(cell_value):
                            cell_value = numconv.convToFloat(cell_value)
                        data_item[headers[i]] = cell_value   
                        i += 1                              
                    else:                                    
                        self.log.warn("Row data skipped - no header for this data..." + str(cell_value))
                parsed_sheet_data.append(data_item)
            
            self.parsed_raw_data[sheet] = parsed_sheet_data
        self._wrapsitedata(self.parsed_raw_data)    
        return self.site_data 

    def _wrapsitedata(self, data):
        for sitename,service_data in data.items():  
            site = SiteData(sitename)
            for service in service_data:     # list

                if service and "Service" in service.keys():

                    # TODO:  This should be a one liner, calling a function that takes a list of maps and turns them into

                    servicename = service["Service"]  # sucky part 1- tied to this spreadsheet type because of how i collected the data
                    for k,v in service.items():          # maps
                        if k != "Service":            # sucky part 2 - so that i don't add in the servicename as a data, though i could without issue, it's just extraneous
                            site.addServiceData(servicename, k, v)
            self.site_data.append(site)

