import sys
import os
from glob import glob
from utils import logger
from load import excelloader
from parse import ch_parser
from report import report_gen

LOG = logger.Logger()
SUPPORTED_FILE_EXTENSIONS = "*.xlsx"  # TODO: Make this a list and change the below logic to loop through and extend or append lists from different globs

def getAllFiles(datadir, datafiles):
    LOG.info("Data Files = " + str(datafiles))
    files = []
    try:
        # check filenames only
        files = [os.path.abspath(f) for f in glob(os.path.abspath(datadir) + os.sep + SUPPORTED_FILE_EXTENSIONS) if f.rpartition(os.sep)[2] in datafiles]
        if len(files) <= 0:
            # check full paths
            files = [os.path.abspath(f) for f in glob(os.path.abspath(datadir) + os.sep + SUPPORTED_FILE_EXTENSIONS) if f in datafiles]
        if len(files) <= 0:
            raise ValueError("Cannot find files based on sourceDir and sourceFiles provided.")
    except Exception as e:
        LOG.error("Error occured in processing input files = " + str(e))
        x = [os.path.abspath(f) for f in glob(os.path.abspath(datadir) + os.sep + SUPPORTED_FILE_EXTENSIONS)]
        LOG.error("The files obtained from 'sourceDir' and 'sourceFiles' was: " + str(datafiles))
        LOG.error("The files currently exising in the sourceDir are: " + str(x))
    finally: 
        LOG.info("Got " + str(len(files)) + " files.")
    
    return files

def getParam(param):
    if "=" in str(param):
        value = str(param).split("=")[1]
        if "," in value:
            return value.split(",")
        else:
            return value
    else:
        return str(param)

def showUsage():
    print("Usage: ")
    print("> python -m main.main -sourceDir=\"dir\" -sourceDiles=\"file1,file2,...\" -reportFile=\"filename\"")

def run(datadir, datafiles, reportfile):
    files = getAllFiles(datadir, datafiles)
    if files and len(files) > 0:

        loader = excelloader.ExcelLoader()
        parser = ch_parser.Parser()

        for f in files:
            # load the file
            LOG.info("Loading report files from file: " + f)
            loader.load_excel_file(f)
            LOG.info("Loaded report files from file: " + f)

            # parse the file
            LOG.info("Parsing report files from file: " + f)
            parser.parse_chrows(loader.get_data())
            LOG.info("Parsed report files from file: " + f)
        
        # build the report if there is something to build
        if parser.site_data and len(parser.site_data) > 0:
            reporter = report_gen.Reporter()
                    
            # report to file
            LOG.info("Generating report from file: " + f)
            reporter.generate_report(parser.site_data, reportfile)   
            LOG.info("Generated report from file: " + f)


    else:
        LOG.error("No files provided for processing.")


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Missing arguments.")
        showUsage()
    else:
        datadir = getParam(sys.argv[1])
        if not os.path.exists(datadir):
            print("Data Directory does not exist: " + str(datadir))
            showUsage()
        datafiles = getParam(sys.argv[2])
        if datafiles is not str and len(datafiles) > 0:
            outputfile = getParam(sys.argv[3])
            outputdir = str(outputfile).rpartition("/")[0]
            if not os.path.exists(outputdir):
                print("Output Directory does not exist: " + str(outputdir))
            else:
                run(datadir, datafiles, outputfile)
        else:
            print("Source Files parameter is incorrect, either a string or is empty! => " + datafiles)