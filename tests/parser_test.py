import unittest
import os
from parse import ch_parser
from utils import logger
from load import excelloader

class ParserTest(unittest.TestCase):

    DIR = 'data'
    FLAT_FILE_EXAMPLE = "Flat-Example.xlsx"
    log = logger.Logger()
   

    def test_parse(self):

        self.log.info("Loading data...")
        
        # first load an excel file and data
        loader = excelloader.ExcelLoader()
        loader.load_excel_file(self.DIR + os.path.sep + self.FLAT_FILE_EXAMPLE)

        self.log.info("Parsing data...")

        # next parse the data
        parser = ch_parser.Parser()
        parser.parse_chrows(loader.get_data())

        self.log.info("Parsed Data = ")
        self.log.info(parser.parsed_raw_data)
        self.log.info("Site Data = ")
        self.log.info(parser.site_data)
