import unittest
from parse import ch_parser
from utils import logger
from load import excelloader
from report import report_gen

class ReportTest(unittest.TestCase):

    FLAT_FILE_EXAMPLE = "data/AWS-Nabors.xlsx"
    log = logger.Logger()

    def test_reportgen(self):
        self.log.info("Loading data...")
        
        # first load an excel file and data
        loader = excelloader.ExcelLoader()
        loader.load_excel_file(self.FLAT_FILE_EXAMPLE)

        self.log.info("Parsing data...")

        # next parse the data
        parser = ch_parser.Parser()
        parser.parse_chrows(loader.get_data())

        self.log.info("Parsed Data = ")
        self.log.info(parser.site_data)

        # finally test the report generation
        self.log.info("Generating Report...")
        reporter = report_gen.Reporter()

        # report to log
        reporter.generate_report(parser.site_data)   

        # report to file
        path = "data/report.xlsx"
        reporter.generate_report(parser.site_data, path)     
