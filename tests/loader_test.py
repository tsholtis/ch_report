import unittest
import os
from load import excelloader
from utils import logger

class LoaderTest(unittest.TestCase):
    DIR = "data"
    FLAT_FILE_EXAMPLE = "Flat-Example.xlsx"
    CLOUD_PROBE_EXAMPLE = "CloudProbe(new).xlsx"

    log = logger.Logger()

    def test_load(self):
        loader = excelloader.ExcelLoader()
        loader.load_excel_file(self.DIR + os.sep + self.FLAT_FILE_EXAMPLE)
        loader.display_excel_data()


if __name__ == '__main__':
    unittest.main()